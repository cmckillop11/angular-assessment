import { Component, OnInit } from '@angular/core';
import { PlaceholderService } from './placeholder.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  //models for component 
  people
  frmId:number = 1;
  whichCategory:string = 'people'
  categories:Object[] = [{cat:'people'},{cat:'planets'},{cat:'vehicles'},{cat:'species'},{cat:'starships'}]
  model

  //functions
  constructor(private placeholderService:PlaceholderService){
  }

  handleClick(){
    this.placeholderService.getParamData(this.frmId, this.whichCategory)
      .subscribe((result) => {this.model = result})
  }

  invokeService(){
    this.placeholderService.getData().subscribe((result)=>{
      this.people = result
      console.log(result)})
  }

  ngOnInit(){
    this.invokeService()
  }

}
