import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { stringify } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root'
})
export class PlaceholderService {

  apiUrl:string = 'https://swapi.co/api/'

  //create a service
  constructor(private http:HttpClient) { }

  //methods for this service 
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      //Log to console instead
      console.error(error); 
  
      //Let the app keep running by returning an empty result.
      return throwError('Something bad happened, please try again later');
      
      //Error message to display to user to screen
      //return of(result as T);
    };
  }

  getData(){
    return this.http.get(`${this.apiUrl}people`).pipe(
      catchError(this.handleError<Object[]>('getData', []))
    );
  }

  getParamData(id, categories){
    return this.http.get(`${this.apiUrl}${categories}/${id}`).pipe(
      catchError(this.handleError<Object[]>('getParamData', []))
    )
  }

}
